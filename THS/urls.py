"""THS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, url
    2. Add a URL to urlpatterns:  url('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Dashboard import views as viewsDashboard
from ClientArea import views as viewsClient
urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/', viewsDashboard.about, name='about'),
    path('contact',viewsDashboard.contact, name='contact'),
    path('clients',viewsDashboard.clients, name='clients'),
    path('products',viewsDashboard.products, name='products'),
    path('',viewsDashboard.index, name='index'),
    path('login',viewsClient.user_login, name='login'),
    path('logout',viewsClient.user_logout, name='logout'),
    path('signin',viewsClient.signin, name='signin'),
    path('private/',viewsClient.private, name='private'),
    
]
