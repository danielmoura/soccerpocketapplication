from django import forms

class EmailForm(forms.ModelForm):
    contact_email = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control'}),label="E-mail")
    contact_name = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control'}),label="Nome")
    content = forms.CharField(required=True,widget=forms.Textarea(attrs={'class': 'form-control'}),label="Mensagem")
    class Meta():
        fields = ('contact_email','contact_name','content')
