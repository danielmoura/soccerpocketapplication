from django.shortcuts import render
from django.http import HttpResponse
from Dashboard.forms import EmailForm
from django.core.mail import send_mail
# Create your views here.
from django.shortcuts import render
import requests
import simplejson as json


def index(request):
    return render(request, 'Landing/index.html')


def about(request):
    return render(request, 'Landing/about.html')

# def contact(request):
#     return render(request,'Landing/contact.html')


def products(request):
    return render(request, 'Landing/products.html')


def clients(request):

    return render(request, 'Landing/clients.html')


def sendEmail(request):
    send_mail(
        'Subject here',
        'Here is the message.',
        'damouracunha@gmail.com',
        ['damouracunha@gmail.com'],
        fail_silently=False,
    )


def contact(request):
    if request.method == 'POST':
        contact_name = request.POST.get(
            'contact_name', '')
        contact_subject = request.POST.get(
            'contact_subject', '')
        contact_email = request.POST.get(
            'contact_email', '')
        contact_content = request.POST.get('contact_content', '')

        send_mail(
            contact_subject,
            "Mensagem enviada por "+contact_name+": "+contact_content,
            contact_email,
            ['fabrica.ths@gmail.com'],
            fail_silently=False,
        )
    return render(request, 'Landing/contact.html')
