from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from ClientArea.forms import UserForm

from django.contrib.auth import authenticate,logout,login
from django.urls import reverse
from django.contrib.auth.decorators import login_required
# JSON/API handlers
from django.shortcuts import render
import requests
import simplejson as json


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('usuario')
        password = request.POST.get('senha')
        if(username=='admin' and password=='admin'):
            return HttpResponseRedirect(reverse('private'))
        # if user is not None:
        #     if user.is_active:
        #         login(request,user)
        #         return HttpResponseRedirect(reverse('private'))
        #     else:
        #         return HttpResponse("Acc not active")
        # else: 
        #     return HttpResponse("Acc not active")
    else:
        if request.user.is_active:
            return HttpResponseRedirect(reverse('private'))
        else:
            return render(request, 'ClientArea/login.html')

def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


def signin(request):
    if request.method == "GET":
        if request.user.is_active:
            return HttpResponseRedirect(reverse('private'))
        else:
            user_form = UserForm()
    else:
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            print(user_form.__dict__['cleaned_data'])
            response = requests.post('http://localhost:8080/api/users',json=user_form.__dict__['cleaned_data'])
            user = response.json()
            print(user)
            return render(request, 'ClientArea/login.html')
            # user = user_form.save()
            # user.set_password(user.password)
            # user.save()
        else:
            print(user_form.errors)
    return render(request, 'ClientArea/signin.html',
                  {'user_form': user_form})

def private(request):
    return render(request, 'ClientArea/private.html',
                  {'first_name': 'admin'})
