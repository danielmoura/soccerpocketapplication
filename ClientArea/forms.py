from django import forms
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),label="Senha")
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),label="E-mail")
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),label="Nome")
    class Meta():
        model = User
        fields = ('name','email','password')

